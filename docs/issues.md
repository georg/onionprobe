# Known issues

From Stem:

* [Python 3.9 warning · Issue #105 · torproject/stem](https://github.com/torproject/stem/issues/105)
* [noisy log: stem: INFO: Error while receiving a control message (SocketClosed): received exception "peek of closed file" · Issue #112 · torproject/stem · GitHub](https://github.com/torproject/stem/issues/112)

Check also the [issue tracker](https://gitlab.torproject.org/tpo/onion-services/onionprobe/-/issues).
