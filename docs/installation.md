# Installation

Onionprobe is [available on PyPI](https://pypi.org/project/onionprobe):

    pip install onionprobe

A package should be available for [Debian bookworm][].

    sudo apt-get install onionprobe

An Arch Linux package is [available on AUR][][^arch-linux-package]:

It's also possible to run it directly from the Git repository:

    git clone https://gitlab.torproject.org/tpo/onion-services/onionprobe
    cd onionprobe

[Debian bookworm]: https://www.debian.org/releases/bookworm/
[available on AUR]: https://aur.archlinux.org/packages/onionprobe-git
[^arch-linux-package]: See also https://gitlab.torproject.org/tpo/onion-services/onionprobe/-/issues/16
