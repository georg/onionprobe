# Helper scripts

## Manpage generator

::: packages.manpage
    options:
      show_root_toc_entry: false
      heading_level: 3

## Real-world Onion Sites

::: packages.real-world-onion-sites
    options:
      show_root_toc_entry: false
      heading_level: 3

## SecureDrop

::: packages.securedrop
    options:
      show_root_toc_entry: false
      heading_level: 3

## TPO

::: packages.tpo
    options:
      show_root_toc_entry: false
      heading_level: 3
